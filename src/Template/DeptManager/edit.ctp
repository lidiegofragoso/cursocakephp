<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DeptManager $deptManager
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Form->postLink(
                __('Borrar'),
                ['action' => 'delete', $deptManager->emp_no],
                ['confirm' => __('¿Estás seguro de eliminar # {0}?', $deptManager->emp_no)]
            )
        ?></li>
        <li><?= $this->Html->link(__('Lista Depto. Managers'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="deptManager form large-9 medium-8 columns content">
    <?= $this->Form->create($deptManager) ?>
    <fieldset>
        <legend><?= __('Editar Dept Manager') ?></legend>
        <?php
            echo $this->Form->control('emp_no',['label' => 'No. Empleado', 'type' => 'number']);
            echo $this->Form->control('dept_no',['label' => 'No. Departamento', 'type' => 'text']);
            echo $this->Form->control( 'from_date',[
                'label' => 'Fecha Inicio',
                'type' => 'date',
                'empty' => true,
                'minYear' => date('Y') - 50,
                'maxYear' => date ('Y') + 50,
                'monthNames' => false
            ]);
            echo $this->Form->control('to_date',[
                'label' => 'Fecha Fin',
                'empty' => true,
                'minYear' => date('Y') - 50,
                'maxYear' => date ('Y') + 50,
                'monthNames' => false
            ]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
