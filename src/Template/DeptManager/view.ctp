<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DeptManager $deptManager
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Editar Depto. Manager'), ['action' => 'edit', $deptManager->emp_no]) ?> </li>
        <li><?= $this->Form->postLink(__('Borrar Depto. Manager'), ['action' => 'delete', $deptManager->emp_no], ['confirm' => __('¿Estás seguro de eliminar # {0}?', $deptManager->emp_no)]) ?> </li>
        <li><?= $this->Html->link(__('Lista Depto. Managers'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nuevo Depto. Manager'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="deptManager view large-9 medium-8 columns content">
    <h3><?= h($deptManager->emp_no) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('No. Departamento') ?></th>
            <td><?= h($deptManager->dept_no) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('No. Empleado') ?></th>
            <td><?= $this->Number->format($deptManager->emp_no) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha Inicio') ?></th>
            <td><?= h($deptManager->from_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha Fin') ?></th>
            <td><?= h($deptManager->to_date) ?></td>
        </tr>
    </table>
</div>
