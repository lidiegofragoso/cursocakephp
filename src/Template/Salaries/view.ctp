<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Salary $salary
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Editar Salario'), ['action' => 'edit', $salary->emp_no]) ?> </li>
        <li><?= $this->Form->postLink(__('Borrar Salario'), ['action' => 'delete', $salary->emp_no], ['confirm' => __('¿Estás seguro de eliminar el registro # {0}?', $salary->emp_no)]) ?> </li>
        <li><?= $this->Html->link(__('Lista de Salarios'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nuevo Salario'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="salaries view large-9 medium-8 columns content">
    <h3><?= h($salary->emp_no) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('No. Empleado') ?></th>
            <td><?= $this->Number->format($salary->emp_no) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Salario') ?></th>
            <td><?= $this->Number->format($salary->salary) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha Inicio') ?></th>
            <td><?= h($salary->from_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha Fin') ?></th>
            <td><?= h($salary->to_date) ?></td>
        </tr>
    </table>
</div>
