<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Salary $salary
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Borrar'),
                ['action' => 'delete', $salary->emp_no],
                ['confirm' => __('Are you sure you want to delete # {0}?', $salary->emp_no)]
            )
        ?></li>
        <li><?= $this->Html->link(__('Lista de Salarios'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="salaries form large-9 medium-8 columns content">
    <?= $this->Form->create($salary) ?>
    <fieldset>
        <legend><?= __('Editar Salario') ?></legend>
        <?php
            echo $this->Form->control('emp_no',['label' => 'No. Empleado', 'type' => 'number']);
            echo $this->Form->control('salary',['label' => 'Salario', 'type' => 'number']);
            echo $this->Form->control( 'from_date',[
                'label' => 'Fecha Inicio',
                'type' => 'date',
                'empty' => true,
                'minYear' => date('Y') - 50,
                'maxYear' => date ('Y') + 50,
                'monthNames' => false
            ]);
            echo $this->Form->control('to_date',[
                'label' => 'Fecha Fin',
                'empty' => true,
                'minYear' => date('Y') - 50,
                'maxYear' => date ('Y') + 50,
                'monthNames' => false
            ]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Enviar')) ?>
    <?= $this->Form->end() ?>
</div>
