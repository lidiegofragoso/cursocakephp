<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Department $department
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Editar Departamento'), ['action' => 'edit', $department->dept_no]) ?> </li>
        <li><?= $this->Form->postLink(__('Borrar Departamento'), ['action' => 'delete', $department->dept_no], ['confirm' => __('Are you sure you want to delete # {0}?', $department->dept_no)]) ?> </li>
        <li><?= $this->Html->link(__('Lista Departamentos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nuevo Departamentos'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="departments view large-9 medium-8 columns content">
    <h3><?= h($department->dept_no) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('No. Departamento') ?></th>
            <td><?= h($department->dept_no) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Nombre Departamento') ?></th>
            <td><?= h($department->dept_name) ?></td>
        </tr>
    </table>
</div>
