<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Department $department
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Form->postLink(
                __('Borrar'),
                ['action' => 'delete', $department->dept_no],
                ['confirm' => __('¿Estás seguro de eliminar # {0}?', $department->dept_no)]
            )
        ?></li>
        <li><?= $this->Html->link(__('Lista Departamentos'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="departments form large-9 medium-8 columns content">
    <?= $this->Form->create($department) ?>
    <fieldset>
        <legend><?= __('Editar Departamento') ?></legend>
        <?php
            echo $this->Form->control('dept_name', ['label' => 'Nombre', 'type' => 'text']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Enviar')) ?>
    <?= $this->Form->end() ?>
</div>
