<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Title $title
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Lista de Titulos'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="titles form large-9 medium-8 columns content">
    <?= $this->Form->create($title) ?>
    <fieldset>
        <legend><?= __('Añadir nuevo título') ?></legend>
        <?php
            echo $this->Form->control('emp_no', ['label' => 'No. Empleado', 'type' => 'number']);
            echo $this->Form->control('title', ['label' => 'Titulo', 'type' => 'text']);
            echo $this->Form->control('from_date', [
                'label' => 'Fecha Inicio',
                'type' => 'date',
                'empty' => true,
                'minYear' => date('Y') - 50,
                'maxYear' => date ('Y') + 50,
                'monthNames' => false
                ]);
            echo $this->Form->control('to_date', [
                'label' => 'Fecha Fin',
                'empty' => true,
                'minYear' => date('Y') - 50,
                'maxYear' => date ('Y') + 50,
                'monthNames' => false
                ]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Enviar')) ?>
    <?= $this->Form->end() ?>
</div>
