<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DeptEmp $deptEmp
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Editar Empleado de Depto.'), ['action' => 'edit', $deptEmp->emp_no]) ?> </li>
        <li><?= $this->Form->postLink(__('Borrar Empleado de Depto.'), ['action' => 'delete', $deptEmp->emp_no], ['confirm' => __('Are you sure you want to delete # {0}?', $deptEmp->emp_no)]) ?> </li>
        <li><?= $this->Html->link(__('Lista de Empleados de Deptos.'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nuevo Empleado de Depto.'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="deptEmp view large-9 medium-8 columns content">
    <h3><?= h($deptEmp->emp_no) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('No.Departamento') ?></th>
            <td><?= h($deptEmp->dept_no) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('No. Empleado') ?></th>
            <td><?= $this->Number->format($deptEmp->emp_no) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha Inicio') ?></th>
            <td><?= h($deptEmp->from_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha Fin') ?></th>
            <td><?= h($deptEmp->to_date) ?></td>
        </tr>
    </table>
</div>
