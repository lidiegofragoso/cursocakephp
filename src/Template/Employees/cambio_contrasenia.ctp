<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Employee $employee
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Cancelar'), ['controller' => 'Pages', 'action' => 'home']) ?></li>
    </ul>
</nav>
<div class="employees form large-9 medium-8 columns content">
    <?= $this->Form->create($employee) ?>
    <fieldset>
        <legend><?= __('cambio Contraseña') ?></legend>
        <p>Usuario: <?= h($employee->email); ?></p>
        <?php
            
            echo $this->Form->control('password', [
                'label' => 'Contraseña Nueva', 
                'type' => 'password', 
                'value' => ''
            ]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Guardar')) ?>
    <?= $this->Form->end() ?>
</div>
