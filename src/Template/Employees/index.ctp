<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Employee[]|\Cake\Collection\CollectionInterface $employees
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Nuevo Empleado'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Lista de Empleados dpto. finanzas'), ['action' => 'empFinance']) ?></li>
    </ul>
</nav>
<div class="employees index large-9 medium-8 columns content">
    <h3><?= __('Empleados') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('emp_no', 'No.Empleado') ?></th>
                <th scope="col"><?= $this->Paginator->sort('birth_date', 'Fecha Nacimiento') ?></th>
                <th scope="col"><?= $this->Paginator->sort('first_name', 'Nombre') ?></th>
                <th scope="col"><?= $this->Paginator->sort('last_name', 'Apellido') ?></th>
                <th scope="col"><?= $this->Paginator->sort('gender', 'Género') ?></th>
                <th scope="col"><?= $this->Paginator->sort('hire_date', 'Fecha Contratación') ?></th>
                <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($employees as $employee): ?>
            <tr>
                <td><?= $this->Number->format($employee->emp_no) ?></td>
                <td><?= h($employee->birth_date) ?></td>
                <td><?= h($employee->first_name) ?></td>
                <td><?= h($employee->last_name) ?></td>
                <td><?= h($employee->gender) ?></td>
                <td><?= h($employee->hire_date) ?></td>
                <td><?= h($employee->email) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $employee->emp_no]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $employee->emp_no]) ?>
                    <?= $this->Form->postLink(__('Borrar'), ['action' => 'delete', $employee->emp_no], ['confirm' => __('¿Estás seguro de eliminar el reigistro?')]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php echo $this->element('Paginador'); ?>
</div>

