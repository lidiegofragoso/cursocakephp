<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DeptEmp Controller
 *
 * @property \App\Model\Table\DeptEmpTable $DeptEmp
 *
 * @method \App\Model\Entity\DeptEmp[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DeptEmpController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $deptEmp = $this->paginate($this->DeptEmp);

        $this->set(compact('deptEmp'));
    }
 
    /**
     * View method
     *
     * @param string|null $id Dept Emp id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null, $dept_no = null)
    {
        $deptEmp = $this->DeptEmp->get([$id, $dept_no]);

        $this->set('deptEmp', $deptEmp);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $deptEmp = $this->DeptEmp->newEntity();
        if ($this->request->is('post')) {
            $deptEmp = $this->DeptEmp->patchEntity($deptEmp, $this->request->getData());
            if ($this->DeptEmp->save($deptEmp)) {
                $this->Flash->success(__('El empleado del departamento ha sido guardado'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El empleado del departamento no pudo ser guardado'));
        }
        $this->set(compact('deptEmp'));
    }

    /** 
     * Edit method
     *
     * @param string|null $id Dept Emp id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null, $dept_no = null)
    {
        //Se obtiene la información de un registro dependiendo de sus llaves primarias.
        $deptEmp = $this->DeptEmp->get([$id, $dept_no]);

        //Se pregunta el tipo de petición
        if ($this->request->is(['patch', 'post', 'put'])) {
            /*Se crea una nueva entidad y se elimina la anterior, ya que si solo se hiciera el patchEntity.
            solo se guardarían los cambos de las columnas que no sean llaves primarias*/
            $newDeptEmp = $this->DeptEmp->newEntity();
            $this->DeptEmp->delete($deptEmp);
            //A partir del registro que queramos editar, se sobrescribe la información que se haya cambiado
            $newDeptEmp = $this->DeptEmp->patchEntity($newDeptEmp, $this->request->getData());
            //Se pregunta si se guardó de forma correcta el registro
            if ($this->DeptEmp->save($deptEmp)) {
                //Se muestra un mensaje de que todo salió correctamente
                $this->Flash->success(__('El empleado del departamento ha sido guardado'));
                //Se redirige la página al index de este mismo controlador
                return $this->redirect(['action' => 'index']);
            }
            //En caso de que no se haya podido guardar el registro se muestra un mensaje de error
            $this->Flash->error(__('El empleado del departamento no pudo ser guardado'));
        }
        //Se manda la información del título a editar a la vista
        $this->set(compact('deptEmp'));
    }

    /** 
     * Delete method
     *
     * @param string|null $id Dept Emp id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null, $dept_no = null)
    {
        //Se restringen que los tipos de peticiones pueden llamar este método
        $this->request->allowMethod(['post', 'delete']);
        //Se obtiene la información de un registro dependiendo de sus llaves primarias.
        $deptEmp = $this->DeptEmp->get([$id, $dept_no]);
        //Se pregunta si se eliminó de forma correcta el registro
        if ($this->DeptEmp->delete($deptEmp)) {
            //Se muestra un mensaje de que todo salió correctamente
            $this->Flash->success(__('El empleado del departamento ha sido eliminado'));
        } else {
            //En caso de que no se haya podido guardar el registro se muestra un mensaje de error
            $this->Flash->error(__('El empleado del departamento no pudo ser eliminado'));
        }
        //Se redirige la página al index de este mismo controlador
        return $this->redirect(['action' => 'index']);
    }
}
