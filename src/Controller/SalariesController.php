<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Salaries Controller
 *
 * @property \App\Model\Table\SalariesTable $Salaries
 *
 * @method \App\Model\Entity\Salary[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SalariesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $salaries = $this->paginate($this->Salaries);

        $this->set(compact('salaries'));
    }

    /**
     * View method
     *
     * @param string|null $id Salary id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null, $from_date = null)
    {
        $salary = $this->Salaries->get([$id, $from_date]);

        $this->set('salary', $salary);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $salary = $this->Salaries->newEntity();
        if ($this->request->is('post')) {
            $salary = $this->Salaries->patchEntity($salary, $this->request->getData());
            if ($this->Salaries->save($salary)) {
                $this->Flash->success(__('Salario guardado'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El salario no pudo ser guardado, intenta de nuevo.'));
        }
        $this->set(compact('salary'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Salary id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null, $from_date = null)
    {
        //Se obtiene información de un registro
        $salary = $this->Salaries->get([$id, $from_date]);

        //Pregunta del tipo de petición
        if ($this->request->is(['patch', 'post', 'put'])) {
            //Creación de una nueva entidad y eliminación de la anterior
            $newSalary = $this->Salaries->newEntity();
            $this->Salaries->delete($salary);
            //Sobreescribir la info que se haya cambiado, a partit del registro que editemos
            $newSalary = $this->Salaries->patchEntity($newSalary, $this->request->getData());
            //pregunta para saber si se guardó correctamente el regitro
            if ($this->Salaries->save($newSalary)) {
                //Mensaje de que todo salió bien
                $this->Flash->success(__('Salario guardado.'));
                //Se redirige la página al index de este mismo controlador
                return $this->redirect(['action' => 'index']);
            }
            //Si no se guardó el regstro, muestra un error
            $this->Flash->error(__('El salario no pudo ser guardado, intenta de nuevo.'));
        }
        //Se manda la información del salario a editar a la vista
        $this->set(compact('salary'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Salary id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null, $from_date = null)
    {
        //Restricción de tipo de peticiones que pueden llegar a este método
        $this->request->allowMethod(['post', 'delete']);
        //Se muestra la información de un registro dependiendo las llaves primarias
        $salary = $this->Salaries->get([$id, $from_date]);
        //Saber si se eliminó correctamente
        if ($this->Salaries->delete($salary)) {
            //Mensaje de eliminación correcta
            $this->Flash->success(__('Salario eliminado'));
        } else {
            //mensaje en caso de no haberse guardado correctamente
            $this->Flash->error(__('El salario no pudo ser eliminado, intenta de nuevo.'));
        }
        //Redirige la página al index del controlador
        return $this->redirect(['action' => 'index']);
    }
}
