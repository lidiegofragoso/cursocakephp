<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DeptManager Controller
 *
 * @property \App\Model\Table\DeptManagerTable $DeptManager
 *
 * @method \App\Model\Entity\DeptManager[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DeptManagerController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $deptManager = $this->paginate($this->DeptManager);

        $this->set(compact('deptManager'));
    }

    /**
     * View method
     *
     * @param string|null $id Dept Manager id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null, $dept_no = null)
    {
        $deptManager = $this->DeptManager->get([$id, $dept_no]);

        $this->set('deptManager', $deptManager);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $deptManager = $this->DeptManager->newEntity();
        if ($this->request->is('post')) {
            $deptManager = $this->DeptManager->patchEntity($deptManager, $this->request->getData());
            if ($this->DeptManager->save($deptManager)) {
                $this->Flash->success(__('El manager del departamento ha sido guardado'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El manager del departamento no pudo ser guardado'));
        }
        $this->set(compact('deptManager'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Dept Manager id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null, $dept_no = null)
    {
        //Se obtiene la información de un registro dependiendo de sus llaves primarias.
        $deptManager = $this->DeptManager->get([$id, $dept_no]);

        //Se pregunta el tipo de petición
        if ($this->request->is(['patch', 'post', 'put'])) {
            /*Se crea una nueva entidad y se elimina la anterior, ya que si solo se hiciera el patchEntity.
            solo se guardarían los cambos de las columnas que no sean llaves primarias*/
            $newDeptManager = $this->DeptManager->newEntity();
            $this->DeptManager->delete($deptManager);
            //A partir del registro que queramos editar, se sobrescribe la información que se haya cambiado
            $newDeptManager = $this->DeptManager->patchEntity($newDeptManager, $this->request->getData());
            //Se pregunta si se guardó de forma correcta el registro
            if ($this->DeptManager->save($newDeptManager)) {
                //Se muestra un mensaje de que todo salió correctamente
                $this->Flash->success(__('El manager del departamento ha sido guardado.'));
                //Se redirige la página al index de este mismo controlador
                return $this->redirect(['action' => 'index']);
            }
            //En caso de que no se haya podido guardar el registro se muestra un mensaje de error
            $this->Flash->error(__('El manager del departamento no pudo ser guardado'));
        }
        //Se manda la información del título a editar a la vista
        $this->set(compact('deptManager'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Dept Manager id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null, $dept_no = null)
    {
        //Se restringen que los tipos de peticiones pueden llamar este método
        $this->request->allowMethod(['post', 'delete']);
        //Se obtiene la información de un registro dependiendo de sus llaves primarias.
        $deptManager = $this->DeptManager->get([$id, $dept_no]);
        //Se pregunta si se eliminó de forma correcta el registro
        if ($this->DeptManager->delete($deptManager)) {
            //Se muestra un mensaje de que todo salió correctamente
            $this->Flash->success(__('Manager de departamento eliminado'));
        } else {
            //En caso de que no se haya podido guardar el registro se muestra un mensaje de error
            $this->Flash->error(__('El manager del departamento no pudo ser eliminado'));
        }
        //Se redirige la página al index de este mismo controlador
        return $this->redirect(['action' => 'index']);
    }
}
