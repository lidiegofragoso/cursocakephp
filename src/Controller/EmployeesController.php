<?php
namespace App\Controller;

use App\Controller\AppController;
//use Cake\Event\Event;

/**
 * Employees Controller
 *
 * @property \App\Model\Table\EmployeesTable $Employees
 *
 * @method \App\Model\Entity\Employee[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class EmployeesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Salaries');
    }

    //Login
    public function login()
    {
        //$employee = $this->Employees->newEntity();
        if ($this->request->is('post')){
            $employee = $this->Auth->identify();
            if($employee){
                $this->Auth->setUser($employee);
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Nombre de usuario incorrecto, intenta de nuevo'));
        }
        //$this->set(compact('employee'));
    }


    //Logout
    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    public function isAuthorized($user)
    {
        if ($this->request->getParam('action') === 'add'){
            return true;
        }
        return parent :: isAuthorized($user);
    }
    
    public function index()
    {
        $employees = $this->paginate($this->Employees);

        $this->set(compact('employees'));
    }

    /**
     * View method
     *
     * @param string|null $id Employee id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $employee = $this->Employees->get($id, [
            'contain' => [],
        ]);

        $this->set('employee', $employee);
    }
    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $employee = $this->Employees->newEntity();

        if ($this->request->is('post')) {
            $employee = $this->Employees->patchEntity($employee, $this->request->getData());
            //Mandamos llamar la función de ID
            $employee -> emp_no = $this->Employees->nextID()->MaxEmp;
            /*debug($employee);
            exit;*/
            if ($this->Employees->save($employee)) {
                $this->Flash->success(__('Empleado guardado'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No pudo guardarse el empleado'));
        }
        $this->set(compact('employee'));
    }

    //Cambio de contraseña
    public function cambioContraseña(){
    }

    /**
     * Edit method
     *
     * @param string|null $id Employee id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $employee = $this->Employees->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $employee = $this->Employees->patchEntity($employee, $this->request->getData());
            if ($this->Employees->save($employee)) {
                $this->Flash->success(__('Empleado guardado'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No pudo guardarse el empleado'));
        }
        $this->set(compact('employee'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Employee id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $employee = $this->Employees->get($id);
        if ($this->Employees->delete($employee)) {
            $this->Flash->success(__('El empleado ha sido eliminado'));
        } else {
            $this->Flash->error(__('No pudo eliminarse el empleado'));
        }

        return $this->redirect(['action' => 'index']);
    }
    /*
    public function listaMujeres()
    {
        //Se buscan todos los títulos que sean de empleadas mujeres
        $titles = $this->Titles->find()
            ->contain('Employees')
            ->where(['Employees.gender' => 'F']);
        //Se manda la información al componente para que sepa cómo mostrar los datos
        $titulosMujeres = $this->paginate($titles);
        //Se manda la información ya paginada a la vista
        $this->set(compact('titulosMujeres'));
    }
    
    public function listaMujeres()
    {
        $query = $this->Employees->find()
        ->contain('Titles')
        ->where(['Employees.gender' => 'F'])
        -> limit(10);

        $titulosMujeres = $this->paginate($titles);
        //Se manda la información ya paginada a la vista
        $this-set(compact('tituloMujeres'));
    }
*/

public function empFinance()
{
    //Consulta para los empleados del departamento de finance con sueldo mayor a 100,000
    $empleados = $this->Employees->find()
    ->join([
        'table' => 'Salaries',
        'alias' => 's',
        'type' => 'INNER',
        'conditions' => [
            's.salary >' => 100000, 
            's.emp_no = employees.emp_no'
        ]
    ])
    ->join([
        'table' => 'dept_emp',
        'alias' => 'd',
        'type' => 'INNER',
        'conditions' => [
            'd.dept_no' => 'd002',
            'd.emp_no = employees.emp_no'
        ]
    ]);

        $empleFinance = $this->paginate($empleados);
        $this->set(compact('empleFinance'));
}


    //Módulo cambio de contraseña
public function cambioContrasenia(){

    $id = $this->Auth->user('emp_no');
    
    //Queremos llamar al empleado a mediante su id
    $employee = $this->Employees->get($id, [
        'contain' => [],
    ]);
    //Si se le manda una solicitud de registro a un empleado mediante patch, post o put, se ejecutan las instrucciones
    if ($this->request->is(['patch', 'post', 'put'])) {
        //traemos los datos del empleado que se quieran editar
        $employee = $this->Employees->patchEntity($employee, $this->request->getData());
        //Validamos que se haya guardado con éxito
        if ($this->Employees->save($employee)) {
            //En caso de lo contrario
            $this->Flash->success(__('Su contraseña se ha guardado con éxito'));
            //Redirigir a employees (index)
            return $this->redirect(['action' => 'index']);
        }
        //En caso de que nos e guarden con éxito, se mandar un mensaje de error
        $this->Flash->error(__('No se puede cambiar la contraseña. Por favor, intente nuevamente'));
    }
    //Mandar datos de la vista
    $this->set(compact('employee'));;
}

}
