<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Titles Controller
 *
 * @property \App\Model\Table\TitlesTable $Titles
 *
 * @method \App\Model\Entity\Title[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TitlesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Employees');
    }

    public function index()
    {
        $titles = $this->paginate($this->Titles);

        $this->set(compact('titles'));
    }

    /**
     * View method
     *
     * @param string|null $id Title id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null, $title = null, $from_date = null)
    {
        $title = $this->Titles->get([$id, $title, $from_date]);

        $this->set('title', $title);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $title = $this->Titles->newEntity();
        if ($this->request->is('post')) {
            $title = $this->Titles->patchEntity($title, $this->request->getData());
            if ($this->Titles->save($title)) {
                $this->Flash->success(__('Se ha guardado el título'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No se pudo guardar el título, intenta de nuevo.'));
        }
        $this->set(compact('title'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Title id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null, $title = null, $from_date = null)
    {
        //Se obtiene la información de un registro dependiendo de sus llaves primarias.
        $title = $this->Titles->get([$id, $title, $from_date]);

        //Se pregunta el tipo de petición
        if ($this->request->is(['patch', 'post', 'put'])) {
            /*Se crea una nueva entidad y se elimina la anterior, ya que si solo se hiciera el patchEntity.
            solo se guardarían los cambos de las columnas que no sean llaves primarias*/
            $newTitle = $this->Titles->newEntity();
            $this->Titles->delete($title);
            //A partir del registro que queramos editar, se sobrescribe la información que se haya cambiado
            $newTitle = $this->Titles->patchEntity($newTitle, $this->request->getData());
            //Se pregunta si se guardó de forma correcta el registro
            if ($this->Titles->save($newTitle)) {
                //Se muestra un mensaje de que todo salió correctamente
                $this->Flash->success(__('Título guardado'));
                //Se redirige la página al index de este mismo controlador
                return $this->redirect(['action' => 'index']);
            }
            //En caso de que no se haya podido guardar el registro se muestra un mensaje de error
            $this->Flash->error(__('El título no pudo ser guardado, por favor inténtelo nuevamente.'));
        }

        //Se manda la información del título a editar a la vista
        $this->set(compact('title'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Title id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    
    public function delete($id = null, $title = null, $from_date = null)
    {
        //Se restringen que los tipos de peticiones pueden llamar este método
        $this->request->allowMethod(['post', 'delete']);
        //Se obtiene la información de un registro dependiendo de sus llaves primarias.
        $title = $this->Titles->get([$id, $title, $from_date]);
        //Se pregunta si se eliminó de forma correcta el registro
        if ($this->Titles->delete($title)) {
            //Se muestra un mensaje de que todo salió correctamente
            $this->Flash->success(__('Título Eliminado.'));
        } else {
            //En caso de que no se haya podido guardar el registro se muestra un mensaje de error
            $this->Flash->error(__('El título no pudo ser eliminado, por favor inténtelo nuevamente.'));
        }
        //Se redirige la página al index de este mismo controlador
        return $this->redirect(['action' => 'index']);
    }

    public function listaMujeres()
    {
        //Se buscan todos los títulos que sean de empleadas mujeres
        $titles = $this->Titles->find()
            ->contain('Employees')
            ->where(['Employees.gender' => 'F']);
        //Se manda la información al componente para que sepa cómo mostrar los datos
        $titulosMujeres = $this->paginate($titles);
        //Se manda la información ya paginada a la vista
        $this->set(compact('titulosMujeres'));
    }
}
